<!DOCTYPE html>

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <meta charset="utf-8">
  <title>WEB4</title>
  <link rel="stylesheet" href="style.css">
  <style>
    /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
    .error {
      border: 2px solid red;

    }
  </style>
</head>

<body>
  <?php
  if (!empty($messages)) {
    print('<div id="messages">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
      print($message);
    }
    print('</div>');
  }

  // Далее выводим форму отмечая элементы с ошибками классом error
  // и задавая начальные значения элементов ранее сохраненными.
  ?>
  <div class="nameForm m-auto">Регистрация</div>
  <div class="container border rounded-5 shadow">

    <form action="" method="POST" >
    <div class="flex-column">
      <div class="c">
        ФИО:

        <input name="fio" placeholder="Фамилия Имя Отчество" <?php if ($errors['fio']) {
                                                                print 'class="error"';
                                                              } ?> value="<?php print $values['fio']; ?>" />

      </div>
      <div class="c">
        E-mail:

        <input type="email" name="email" placeholder="Введите e-mail" <?php if ($errors['email']) {
                                                                        print 'class="error"';
                                                                      } ?> value="<?php print $values['email']; ?>">

      </div>
      <div class="c">
        <p>Ваш год рождения:</p>
          <input name="year" type="date" <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year_value']; ?>" id="dr">
      </div>
      <div class="c">
        <p>Пол:</p>
        <label>
          <input type="radio" name="radio-pol" checked="checked" value="M">М
        </label>
        <label>
          <input type="radio" name="radio-pol" value="W" />Ж
        </label>

        <p>Количество конечностей</p>
        <label>
          <input type="radio" name="radio-kon" value="0" />0
        </label>
        <label>
          <input type="radio" name="radio-kon" value="1" />1
        </label>
        <label>
          <input type="radio" name="radio-kon" value="2" />2
        </label>
        <label>
          <input type="radio" name="radio-kon" value="3" />3
        </label>
        <label>
          <input type="radio" checked="checked" name="radio-kon" value="4" />4
        </label>
        <label>
          <input type="radio" name="radio-kon" value="5" />5
        </label>
      </div>
      <div class="c">
        <p>Сверхспособности</p>

        <select name="sp-sp[]" multiple=multiple>
          <option selected value="s1">Мега здоровье</option>
          <option value="s2">Знать и не учить</option>
          <option value="s3">Не уставать и не спать</option>
          <option value="s4">5 по дифурам</option>
        </select>

      </div>
      <div class="c">
      <p id="bio">Биография:</p>
      
        <textarea placeholder="Расскажите о себе" name="biography" rows="3" cols="40" <?php if ($errors['biography']) {
            print 'class="error"';
        } ?> ><?php print $values['biography']; ?></textarea>
        </div>
      <div class="c">
      <label>
        С контрактом ознакомлен
        <input type="checkbox" name="check-ok" checked="checked">
      </label>
      </div>
      </div>
      <div class="m-auto button p-3">
      <input type="submit" value="Отправить" class="btn btn-success ">
      </div>
    </form>
  </div>
</body>

</html>
